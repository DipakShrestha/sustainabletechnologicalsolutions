import React, { Component } from 'react';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';

const mapStyles = {
    position: 'absolute',
    width: '100%',
    height: '100%'
};
export class MapContainer extends Component {
    constructor() {
        super();
        this.state = {
            showingInfoWindow: false,  //Hides or the shows the infoWindow
            activeMarker: {},          //Shows the active marker upon click
            selectedPlace: {}          //Shows the infoWindow to the selected place upon a marker
        };
        this.onMarkerClick = this.onMarkerClick.bind(this);
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.onClose = this.onClose.bind(this);
    }

   

    onMarkerClick(props, marker, e) {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: !this.state.showingInfoWindow,
        });
    }
    onClose(props) {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    };

    onMouseEnter(props,marker){
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        })
    }

    render() {
        return (
            <Map
                google={this.props.google}
                zoom={17}
                // styles={mapStyles}
                // calssName="map"
                initialCenter={{
                    lat: 27.675344,
                    lng: 85.365199
                }}>
                <Marker
                    onMouseover={this.onMouseEnter}
                    onClick={this.onMarkerClick}
                    name={'Sustainable Technological Solutions (STS)'} />
                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}
                    onClose={this.onClose}>
                    <div style={{color: 'black', padding:'5px'}}>
                        <div >{this.state.selectedPlace.name}</div>
                        <div>info@stsolutions.com.np</div>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCGPnL2IGAK54ZZnhAsT1v01bDZdzBQQRM'
})(MapContainer);