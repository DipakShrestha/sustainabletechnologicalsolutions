import React, { Component } from 'react';
import { ArrowRight, Eye, Star } from 'react-bytesize-icons';
import { HashRouter as Router, Route, NavLink } from 'react-router-dom';

import '../../../../css/header/dockComponent/dock-component.css';

export default class DockComponent extends Component {
    constructor(props) {
        super(props);
        this.showHideDock = this.showHideDock.bind(this);
        this.ToUpperCase = this.ToUpperCase.bind(this);
        this.state = {
            subMenuServices: [{ key: 's1', value: 'Data' },
            { key: 's2', value: 'Mobile' },
            { key: 's3', value: 'Web' },
            { key: 's4', value: 'Design' }],

            subMenuAbout: [{ key: 'a1', value: 'About Company' },
            { key: 'a2', value: 'Management Team' },
            { key: 'a3', value: 'Web' },
            { key: 'a4', value: 'Design' }],

            subMenuBlog: [{ key: 'b1', value: 'Data' },
            { key: 'b2', value: 'Mobile' },
            { key: 'b3', value: 'Web' },
            { key: 'b4', value: 'Design' }],

            sideMenu: [{ key: 'm1', value: 'Career', to: '/career' },
            { key: 'm2', value: 'PortFolio', to: '/portfolio' },
            { key: 'm4', value: 'Contact', to: '/contact_us' },
            { key: 'm5', value: 'Contact', to: '/contact_us' },
            { key: 'm6', value: 'Contact', to: '/contact_us' }],

            navbarmenu: [],
            navbarMenuMain: [],
            navbarMenuMainLink:[]
        }
    }

    componentWillMount() {
        axios.get('/api/navbarmenu').then(response => {
            this.setState({
                navbarmenu: response.data
            })
        }).catch(errors => {
            console.log(errors);
        })
    }
    componentDidMount() {
        setTimeout(() => {
            this.state.navbarmenu.sort((a, b) => a.position - b.position);
            // console.log(this.state.navbarmenu);
        }, 1500)
    }

    showHideDock(e) {
        this.props.showOrHideDock(e);
    }

    ToUpperCase(str) {
        return str.toUpperCase();
    }

    render() {
        const sideNavMenu = this.state.navbarmenu.map(sideNavMenu => {
            if (sideNavMenu.position <= 2) {
                // sideNavMenu.navbar_menu_item[0] = sideNavMenu.navbar_menu_item[0].toUpperCase();
                this.state.navbarMenuMainLink.push(sideNavMenu.navbar_menu_item.toLowerCase());
                this.state.navbarMenuMain.push(sideNavMenu.navbar_menu_item.toUpperCase());
            }
            if (sideNavMenu.position > 2) {
                return (
                    <NavLink key={sideNavMenu.id} to={'/' + sideNavMenu.navbar_menu_item} id="dock-side-menu" onClick={() => this.showHideDock(false)}>
                        <h2 >
                            {/* <span style={{ marginRight: '3px', color: '#9CF5A6' }} ><Star width={30} height={30} /></span> */}
                            {this.ToUpperCase(sideNavMenu.navbar_menu_item)}
                        </h2>
                    </NavLink>
                )
            }
        })

        const navbarMenuMainLink = this.state.navbarMenuMain.map(navbarMenuMain => {
            navbarMenuMain.toUpperCase();
        })

        const sideMenu = this.state.sideMenu;
        const renderSideMenu = sideMenu.map(sideM => {

            return (
                <NavLink key={sideM.key} to={sideM.to} id="dock-side-menu" onClick={() => this.showHideDock(false)}>
                    <h2 >
                        <span style={{ marginRight: '3px', color: '#9CF5A6' }} ><Star width={30} height={30} /></span>
                        {sideM.value}
                    </h2>
                </NavLink>
            )
        })
        return (
            <div style={{ width: '100%', height: '100%', background: 'transparent' }}>
                {/* <img
                    src={require('../../../../assest/icon/stsLogo.png')}
                    style={{
                        width: '100%',
                        height: '100%',
                        position: 'fixed',
                        zIndex: '2'
                    }}>
                </img> */}
                <div className="dockMainContainer">
                    {/* <div className="gridContainer">                        
                    </div> */}

                    <div className="navMenuContainer">
                        <div className="menuTitle">

                        </div>
                        <div className="menuRow">
                            <div className="menuCol1">
                                <NavItemDetail title={this.state.navbarMenuMain[0]} link={this.state.navbarMenuMainLink[0]} subTitle={this.state.subMenuServices} showHideDock={this.showHideDock} />
                                <span style={{
                                    borderRight: '1px solid rgba(255, 255, 255, 0.6)',
                                    marginTop: '40px',
                                    marginBottom: '80px'
                                }} />
                                <NavItemDetail title={this.state.navbarMenuMain[1]} link={this.state.navbarMenuMainLink[1]} subTitle={this.state.subMenuAbout} showHideDock={this.showHideDock} />
                                {/* <span style={{
                                    borderRight: '1px solid rgba(255, 255, 255, 0.6)',
                                    marginTop: '40px',
                                    marginBottom: '80px'
                                }} />
                                <NavItemDetail title={this.state.navbarMenuMain[2]} link={this.state.navbarMenuMainLink[2]} subTitle={this.state.subMenuBlog} showHideDock={this.showHideDock} /> */}
                            </div>
                            <div className="menuCol2">
                                <div style={{ alignSelf: 'justify' }} >
                                    {/* {renderSideMenu} */}
                                    {sideNavMenu}
                                </div>
                            </div>
                        </div>

                    </div>

                    {/* <Route path="/career" exact component={Header}></Route>
                        <Route path="/about_us" exact component={AboutUs}></Route>
                        <Route path="/services" exact component={Services}></Route>
                        <Route path="/our_work" exact component={OurWork}></Route> */}
                </div>
            </div>
        );
    }
}

class NavItemDetail extends Component {
    constructor(props) {
        super(props);
        this.showHideDock = this.showHideDock.bind(this);
        this.state = {
            msg: 'hello',
            navbarmenuTitle: [],
        }
    }

    showHideDock(e) {
        this.props.showHideDock(e);
    }

    render() {
        const subTitle = this.props.subTitle;

        const renderSubMenu = subTitle.map(submenu => {
            return (
                <NavLink key={submenu.key} to="/test" className="dockSubMenu"
                    id="dock-sub-menu" style={{ color: '#FFFFFF' }}>
                    <h6>
                        <span style={{ marginRight: '3px', color: '#9CF5A6' }}><Eye width={17} height={17} /></span>
                        {submenu.value}
                    </h6>
                </NavLink>

            );
        })

        return (
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                width: '200px',
                height: '80%',
                marginTop: '20px',
                background: 'transparent',
            }}>
                <div style={{
                    width: '200px',
                    height: '80%',
                    background: 'transparent',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    color: 'whitesmoke',
                    marginTop: '25px',
                }}>
                    <NavLink id="dock-sub-menu-title" to={"/" + this.props.link}
                        onClick={() => this.showHideDock(false)}
                        style={{ color: '#FFFFFF' }}>
                        <h3 >
                            <span style={{ marginRight: '3px', color: '#9CF5A6' }} >
                                {/* <Star width={20} height={20} /> */}
                            </span>
                            {this.props.title}
                        </h3>
                    </NavLink>

                    <div style={{
                        width: '200px',
                        background: 'transparent',
                        display: 'flex',
                        marginTop: '20px',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        justifyContent: 'space-evenly'
                    }}>
                        {/* <h6>this.</h6>
                        <h6>Mobile</h6>
                        <h6>Web</h6>
                        <h6>Design</h6> */}
                        {renderSubMenu}

                    </div>

                </div>
                <NavLink id="btn-read-more" to={"/" + this.props.link}
                    onClick={() => this.showHideDock(false)}>
                    <button id="btnReadMore" style={{ borderRadius: '5px', position: 'absolute', bottom: '12%', width: '200px' }}>
                        View All <span id="arrow-left"  ><ArrowRight width={20} /></span>
                    </button>
                </NavLink>
            </div>
        );
    }
}