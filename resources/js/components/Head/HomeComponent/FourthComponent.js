import React, { Component } from 'react';
import '../../../../css/header/header.css';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, {colorPalette} from 'material-icons-react';
export default class FourthComponent extends Component {
    render() {
        return (
            <div className="fullPageSectionFourth">
                {/* <div style={{height:'40px'}}>hello</div> */}
                <div className="sectionFourthContainer">

                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <img src={require('../../../../assest/icon/stsLogo.png')} style={{ width: '65px', height: '65px' }}></img>
                            <div style={{ fontSize: '17px', width: '100%', textAlign: 'center' }}>Sustainable Technological Solutions</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly' }}>
                            <div style={{ display: 'flex', flexDirection: 'row', marginTop: '10px' }}>
                                <Telephone></Telephone>
                                <div style={{ marginLeft: '12px' }}>Phone here</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Mail></Mail>
                                <div style={{ marginLeft: '12px' }}>info@stsolutions.com.np</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Location></Location>
                                <div style={{ marginLeft: '12px' }}>Location here</div>
                            </div>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="perm_phone_msg" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Contact Us</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }} >+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="location_on" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Location Map</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'row', background: 'red', width: '100%', height: '250px', position: 'relative' }}>
                            <div style={{ width: '100%', zIndex: '99999', height: '100%', position: 'absolute', borderStyle: 'solid', borderColor: '#9CF5A6', borderWidth: '1px' }} >
                                <MapContainer />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <div className="textFooter">
                        2019, All Rights Are Reserved By Sustainable Technological Solutions.
                                    </div>
                    <div className="iconFooter">
                        {/* <Twitter className="icon"></Twitter>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon> */}
                        <a href={this.state.hrefFacebook} target="_blank">
                            <SocialIcon className="icon" network="facebook" style={{ width: '30px', height: '30px', color: 'white' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefGoogle} target="_blank">
                            <SocialIcon className="icon" network="google" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hreTwitter} target="_blank">
                            <SocialIcon className="icon" network="twitter" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefInstagram} target="_blank">
                            <SocialIcon className="icon" network="instagram" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}