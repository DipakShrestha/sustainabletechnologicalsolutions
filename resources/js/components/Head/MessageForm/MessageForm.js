import React, { Component } from 'react';
import '../../../../css/header/messageForm/message-form.css';

export default class MessageForm extends Component {
    constructor(props){
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
        this.state={
            user_name:'',
            user_email:'',
            user_message:''
        }
    }

    handleNameChange(e){
        this.setState({
            user_name: e.target.value
        })
    }

    handleEmailChange(e){
        this.setState({
            user_email:e.target.value
        })
    }

    handleMessageChange(e){
        this.setState({
            user_message: e.target.value
        })
    }

    handleMessageSubmit(e){ 
        e.preventDefault(); 
        axios.post('/p', this.state).then(response => {
            console.log(response);
            if (response.status == 200) {
                // alert('Success! Your detail has been sent. Be patient, we will reply you within 24 hours. Thank You!');
                console.log('success');
            }
        }).catch(error => {
            if (error.response) {
                alert('Code: ' + error.response.data.error.code + 
                      '\r\nMessage: ' + error.response.data.error.message);
                console.log('success', error.message); 
              } else {
                console.log('Error', error.message);
              } 
        });
    }

    render() {
        return ( 
            <div  style={{padding:'10px'}}>
                <form className="messageFormContainer needs-validation" onSubmit={this.handleMessageSubmit} >
                    <div className="form-group">
                        <label htmlFor="name-input">Name</label>
                        <input type="name" 
                                className="form-control" 
                                name="user_name" 
                                id="name-input" 
                                placeholder="Name" 
                                value={this.state.user_name}
                                validate={this.validate} required 
                                onChange={this.handleNameChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email-input">Your Email</label>
                        <input type="email" 
                                className="form-control" 
                                id="email-input" name="user_email" 
                                aria-describedby="emailHelp" 
                                placeholder="Enter your email"
                                value={this.state.user_email}
                                validate={this.validate} required 
                                onChange={this.handleEmailChange}/>
                        <small id="emailHelp" className="form-text" style={{color: 'rgba(255,255,255,0.3)'}}>We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label >Message</label>
                        <textarea placeholder="Type your message here" 
                                    name="user_message" 
                                    style={{width:'100%',height:'12vh'}} 
                                    value={this.state.user_message}
                                    validate={this.validate} required 
                                    onChange={this.handleMessageChange}/>
                    </div>
                    
                    <button type="submit" className="btnSubmitMessage" >Send</button>
                </form>
            </div>
        )
    }
}