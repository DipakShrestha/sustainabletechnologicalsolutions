import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ReactFullpage from '@fullpage/react-fullpage';
import '../../../css/aboutus/theme.css';
import { NavLink } from 'react-router-dom';
export default class AboutUs extends Component{
    constructor(props){
      super(props);
      this.fullpageApi=null;
      this.btnrender=this.btnrender.bind(this);
      this.currentState=null;
  		this.state={
  			labelArray:[{value:'Our Mission'},
  						{value:'Management'}
            ],
  			activeBtn:0,
        count : 25
  		}
    }
    componentDidMount(){
			console.log('hello world');
    }
    btnrender(){
  		const renderBtn=this.state.labelArray.map((label,Index)=>{
  				return(
  					<div key={Index}
  						className={this.NavLinkActive(Index)? 'sideBarContent active': 'sideBarContent' }
  						onClick={()=>{this.Navlink(Index);this.fullpageApi.moveTo(Index+1);}}>
  						{label.value}
  					</div>
  				);
  		});
  		return(renderBtn);
  	}
  	Navlink(keyvalue){
  		this.setState({
  				activeBtn:keyvalue
  			});
  	}
  	NavLinkActive(keyid){
  		if(this.state.activeBtn===keyid){
  			return true;
  		}
  		return false;
  	}
    onLeave(origin,destination,direction){
      console.log(destination.index);
      this.setState({activeBtn:destination.index});
    }
		componentDidMount(){
			setTimeout(()=>{
				this.fullpageApi.destroy();
			},2000);
		}
    render(){
        return(
          <div className="aboutus">
            <div className="sideBar">
              <this.btnrender />
            </div>
            <ReactFullpage
              onLeave={this.onLeave.bind(this)}
              render={({ state, fullpageApi }) => {
                this.fullpageApi=fullpageApi;
                this.currentState=state;
                return (
                  <ReactFullpage.Wrapper>
                    <div style={{backgroundImage: `url(${require('../../../assest/image/aboutus/background1.jpg')})`}} className="section">
                      <Theme />
                    </div>
                    <div style={{backgroundImage: `url(${require('../../../assest/image/aboutus/background2.jpg')})`}} className="section">
                      <Management />
                    </div>
                  </ReactFullpage.Wrapper>
                );

              }}
            />
          </div>
        );
    }
}
class Theme extends Component{
	constructor(props) {
        super(props);
        this.btnClicked = this.btnClicked.bind(this);
				this.ContactUsPage='contact';
  }
  btnClicked(){
    	console.log("btnclicked");
  }
	render(){
		return(
				<div className="moto">
					<span className="mainmoto">
						Our mission is to optimize the ideas through innovation
					</span>
					<NavLink className="btn-text" to={'/'+this.ContactUsPage}>
						Lets Do It
					</NavLink>
          <div className="main-text container text-left">
						<h3 className="text-left">About Us</h3>
            Sustinable Technological Solution is the software development and information technology (IT) service support center for STS Analytics. We’re a dedicated team of software and IT professionals who provide technical support to businesses. At STS, we help our internal clients focus on their business solutions while we focus on the technology.
          </div>

				</div>
		);
	}
}
// class OurClient extends Component{
// 	constructor(props) {
//         super(props);
//   }
// 	render(){
// 		return(
// 			<div className='clients'>
//               <div className='clientsMoto'>
//                 <span>Our Relationships often extend beyond the first engagement.</span>
//               </div>
//               <div className='clientsMain'>
//                 <div className='clientsMainLeft'>
//                   <span>Over 75 percent repeat buisness</span>
//                   <span>Clients from healthcare, retail, education, and more</span>
//                   <span>Trusted by renowned brands</span>
//                   <span>Technology partner to globally recognized startups</span>
//                 </div>
//                 <div className='clientsMainRight'>
//                   <div className='clientsMainRightRow'>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                   </div>
//                   <div className='clientsMainRightRow'>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                     <div className='image'>
//                       <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
//                     </div>
//                   </div>
//                   <div className='clientsMainRightLast'>
//                     <span className='clientsMainRightButton'a>View All</span>
//                   </div>
//                 </div>
//               </div>
//             </div>
// 		);
// 	}
// }
class OurOffice extends Component{
	constructor(props){
        super(props);
  }
	render(){
		return(
			<div>
				<div className="moto">
          <div className="motoimage">
            <img src={require('../../../assest/image/mainBack.jpg')}/>
          </div>
          <div className="motoaboutpara">
					     <span className="motopara">
						         Sustinable Technological Solution is the software development and information technology (IT) service support center for STS Analytics. We’re a dedicated team of software and IT professionals who provide technical support to businesses. At STS, we help our internal clients focus on their business solutions while we focus on the technology.
					     </span>
          </div>
				</div>
			</div>
		);
	}
}
class Management extends Component{
	constructor(props) {
        super(props);
        this.btnClicked = this.btnClicked.bind(this);
        this.images = this.images.bind(this);
        this.state={
        	imgname:'1',
			name:'Shyam',
			position:'post-1'
        }
    }
    btnClicked(){
    	console.log("btnclicked");
    }
    images(){
    		return(
    			<div className="teamImage">
    			<img className={this.imgSlideTo('1')? 'active': 'passive' } src={require('../../../assest/image/1.png')} alt="img"/>
    			<img className={this.imgSlideTo('2')? 'active': 'passive' } src={require('../../../assest/image/b.png')} alt="img"/>
    			<img className={this.imgSlideTo('3')? 'active': 'passive' } src={require('../../../assest/image/c.png')} alt="img"/>
    			<img className={this.imgSlideTo('4')? 'active': 'passive' } src={require('../../../assest/image/d.png')} alt="img"/>
    			</div>
    		);
    }
    imgSlider(value,name,post){
    	this.setState({
    		imgname:value,
			name:name,
			position:post
    	});
    }
    imgSlideTo(value){
    	if(this.state.imgname===value){
    		return true;

    	}
    	return false;
    }
	render(){
		return(
			<div style={{overflowX:'hidden'}}className="teamContainer">
				<div className="teamDisplay">

						<this.images />

					<div className="teamInfo">
						<span className="teamMemberName">{this.state.name}</span>
						<span className="teamMemberPosition">{this.state.position}</span>
					</div>
				</div>
				<div className="teamMembers">
					<div className="images">
						<img className={this.imgSlideTo('1')? 'active': '' } src={require('../../../assest/image/a.png')} onClick={()=>this.imgSlider('1','Alish','post')} alt="img"/>
					</div>
					<div className="images">
						<img className={this.imgSlideTo('2')? 'active': '' } src={require('../../../assest/image/b.png')} onClick={()=>this.imgSlider('2','Subas','2-Post')} alt="img"/>
					</div>
					<div className="images">
						<img className={this.imgSlideTo('3')? 'active': '' } src={require('../../../assest/image/c.png')} onClick={()=>this.imgSlider('3','Saurav','3-Post')} alt="img"/>
					</div>
					<div className="images">
						<img className={this.imgSlideTo('4')? 'active': '' } src={require('../../../assest/image/d.png')} onClick={()=>this.imgSlider('4','Subas','4-post')} alt="img"/>
					</div>
				</div>

			</div>
		);
	}
}
