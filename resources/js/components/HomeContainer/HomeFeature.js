import React, { Component } from 'react'
import BannerAnim from 'rc-banner-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import './index.css';
import './thumb.css';
import '../index.css';
import '../../../css/header/home.css';

import mobile from '../../../assest/image/mobileApplication.jpg';
import web from '../../../assest/image/webApplication.jpg';

const { Element, Thumb } = BannerAnim;
const BgElement = Element.BgElement;
export default class HomeFeature extends Component {
    constructor(props) {
        super(props);
        this.imgArray = [
            mobile,
            web
        ];

        this.state = {
            enter: true,
            arrowShow: false,
            autoPlay: false,
            autoPlaySpeed: 5000,
            dragPlay: false,
            serviceContent: 'Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.',

        };
    }

    onMouseEnter() {
        this.setState({
            enter: true,
        });
    }

    onMouseLeave() {
        this.setState({
            enter: true,
        });
    }

    render() {
        const thumbChildren = this.imgArray.map((img, i) =>
            <span className="thumbnail-services" key={i}><i style={{ backgroundImage: `url(${img})` }} /></span>
        );
        return (
            <BannerAnim onMouseEnter={this.onMouseEnter.bind(this)}
                onMouseLeave={this.onMouseLeave.bind(this)}
                arrow={this.state.arrowShow}
                autoPlay={this.state.autoPlay}
                dragPlay={this.state.dragPlay}
                autoPlaySpeed={this.state.autoPlaySpeed}>
                <Element key="aaa"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                              rgba(0, 100, 50, 0.5),
                              rgba(0, 100, 50, 0.5)
                            ),url(${this.imgArray[0]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim" >
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Our Features</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    The Complete Digital Solution
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                                {/* <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    View More
                                </button> */}
                            </div>
                        </div>
                    </QueueAnim>

                </Element>
                <Element key="bbb"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(0, 100, 50, 0.5),
                                rgba(0, 100, 50, 0.5)
                            ),url(${this.imgArray[1]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim">
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Our Features</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Inventive Use of Technology
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                                {/* <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    View More
                                </button> */}
                            </div>
                        </div>
                    </QueueAnim>

                </Element>
                {/* <Element key="ccc"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(255, 0, 0, 0.5),
                                rgba(255, 0, 0, 0.5)
                              ),url(${this.imgArray[1]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim">
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1">Services We Provide</h1>
                            <div style={{
                                width: 'fit-content',
                                alignSelf: 'flex-start',
                                padding: '60px',
                                height: '400px',
                                background: 'transparent'
                            }}>
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Mobile Application Development
                                </TweenOne>
                                <p key="p" style={{ paddingTop: '20px' }}>
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                            </div>
                        </div>
                    </QueueAnim>

                </Element> */}
                <Thumb prefixCls="user-thumb" key="thumb" component={TweenOne}
                    animation={{ bottom: this.state.enter ? 0 : -70 }}
                >
                    {thumbChildren}
                </Thumb>
            </BannerAnim>
        );
    }
}