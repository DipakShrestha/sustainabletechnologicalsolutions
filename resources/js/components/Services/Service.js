import React,{Component} from 'react';
import '../../../css/services/eachservicestyle.css';
import { ChevronRight } from 'react-bytesize-icons';
import { NavLink } from 'react-router-dom';

export default class Service extends Component{
    constructor(props){
      super(props);
      this.state={

      }
    }
    render(){
      return (
          <div className="serviceWrapper">
            <div className="serviceNavBar">
                <div><NavLink to="/services">
                  <span className='header'>Services</span> 
                </NavLink>  /   <span> Quality Assurance And Testing </span> </div>
            </div>
            <div className="serviceHeader">
              <span>Quality Assurance</span>
              <span>And Testing</span>
            </div>
            <div className="serviceContent">
              <span>Customers will not tolerate slow, complicated, or defective applications. Why risk losing customers and compromise your competitive edge? Find issues in your application before users do. Our quality assurance (QA) techniques and agile test strategies will help you launch applications with zero defects. Web and mobile application testing, cloud testing, security testing, quality of data testing, performance testing, automated testing—we have them all covered.</span>
            </div>

            <div className="serviceSubContentMain">

              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Proactive Approach Toward Quality Assurance
                </div>
                <div className="serviceSubContentContent">
                  To ensure seamless end-user experience, the QA process needs to be strategized and proactively applied, integrating appropriate testing methods at each stage. Studying business objectives of the product, we plan out the test environment, process, and tools. Identifying defects ahead in time helps cut down costs and ensure quick, value-driven delivery.
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                 Defect-Free Delivery Through Independent Testing
                </div>
                <div className="serviceSubContentContent">
                  Whether it is functional or non-functional testing, manual or automated approaches, we have experience in testing multiple platforms and complex software systems such as enterprise content management systems or high-traffic eCommerce websites. We also have well-defined QA processes for the different methodologies of software development, be it RUP, agile, or waterfall.
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Testing Methodology
                </div>
                <div className="serviceSubContentContent">
                  Our testing methodology incorporates the continual process of measuring, monitoring, and feedback analysis to ensure that we prevent errors in an ongoing development process. Agile testing methodology, close collaboration with clients, and efficient change management process enable us to quickly roll out quality results.
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Quality Assurance Tools
                </div>
                <div className="serviceSubContentContent">
                  TWe have a dedicated device lab and proprietary tools that enable us to reduce your overhead costs and improve time-to-market. We may also develop plug and play QA solutions based on our client's need. Our team of certified (CSTE, ISTQB, CSQA, CMST) testing professionals will help identify the best solution for your application.
                </div>
              </div>

            </div>
            <div className="testingFirstWrapBorder">
            <div>
                <h3>Functional Testing</h3>
                <div className="testlingtwoList">
                    <ul className="listText listTextCommon">
                        <li>
                            <span className="listCont">Smoke Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Regression Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Mobile CSS Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Automation Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Quality of Data testing</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div>
                <h3 className="second">Non-Functional Testing</h3>
                <div className="testlingtwoList">
                    <ul className="listText listTextCommon">
                        <li>
                            <span className="listCont">Security Testing</span>
                        </li>
                        <li>
                            <span className="listCont">SEO Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Compatibility Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Performance Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Usability Testing</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

          </div>
      );
    }
}



