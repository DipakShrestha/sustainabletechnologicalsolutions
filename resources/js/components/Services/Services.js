import React,{Component} from 'react';
import '../../../css/services/style.css';
export default class Services extends Component{
    render(){
        return(
            <div className='services'>
              <div className='servicesMoto'>
                <span>Our Relationships often extend beyond the first engagement.</span>
              </div>
              <div className='servicesMain'>
                <div className='servicesMainLeft'>
                  <span>Over 75 percent repeat buisness</span>
                  <span>Clients from healthcare, retail, education, and more</span>
                  <span>Trusted by renowned brands</span>
                  <span>Technology partner to globally recognized startups</span>
                </div>
                <div className='servicesMainRight'>
                  <div className='servicesMainRightRow'>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                  </div>
                  <div className='servicesMainRightRow'>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                    <div className='image'>
                      <img src='https://www.technobuzz.net/wp-content/uploads/2013/02/Add-Background-Image-to-Google-Search-Page.jpg' alt='img1'/>
                    </div>
                  </div>
                  <div className='servicesMainRightLast'>
                    <span className='servicesMainRightButton'>View All</span>        
                  </div>
                </div>
              </div>
            </div>
        );
    }
}