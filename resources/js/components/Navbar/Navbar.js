import React, { Component } from 'react';
import { HashRouter as Router, Route, NavLink, Redirect, Switch } from 'react-router-dom';
import Dock from 'react-dock';
import { Close, Menu } from 'react-bytesize-icons';
import Icon from 'react-geomicons';
import '../../../css/navbar/navbar.css';
import '../../../css/extra/border.css';
import '../../../css/extra/line.css';
import '../../../css/extra/animation.css';
import OurWork from '../OurWork/OurWork.js';
import Services from '../Services/Services.js';
import AboutUs from '../AboutUs/AboutUs.js';
import Practice from '../Practice.js';
import Header from '../Head/Header';
import DockComponent from '../Head/DockComponent/DockComponent.js';
import ContactComponent from '../Head/ContactComponent/ContactComponent';
import HomePageComponent from '../Head/HomePageComponent.js';
import PracticeTwo from '../practice2';

// import '../../../sass/animation-border-snake.scss';

export default class Navbar_Component extends Component {
    constructor(props) {
        super(props);
        this.handleNavToggle = this.handleNavToggle.bind(this);
        this.state = {
            isOpen: true,
            isTop: true,
            isBottom: true
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    handleNavToggle(e) {
        this.props.handleToggle(e);
    }
    render() {
        return (
            <div>
                <Navbar_Element handleNavToggle={this.handleNavToggle} />
            </div>
        );
    }
}
export class Navbar_Element extends Component {
    constructor(props) {
        super(props);
        this.handleOverview = this.handleOverview.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.handleNavLinkClick = this.handleNavLinkClick.bind(this);
        // this.conditionalRenderNavbar = this.conditionalRenderNavbar.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
        this.showOrHideDock = this.showOrHideDock.bind(this);
        this.DockHere = this.DockHere.bind(this);
        this.handleDockVisibleChange = this.handleDockVisibleChange.bind(this);
        this.displayDockMenu = this.displayDockMenu.bind(this);
        this.handleSearchIcon = this.handleSearchIcon.bind(this);
        this.capitalFirstLetter = this.capitalFirstLetter.bind(this);
        this.gotoHomPageComponent = this.gotoHomPageComponent.bind(this);

        this.childHomePageComponent = React.createRef();
        this.state = {
            showDock: false,
            size: 0.53,
            fluid: true,
            text: true,
            displayFullNav: true,
            duration: 1000,
            // dimMode: 'opaque',
            dockVisible: false,
            changeSearchIcon: false,
            navbarmenu: [],
            components: [
                { key: 'contact', component: ContactComponent },
                { key: 'about', component: AboutUs },
                { key: 'career', component: OurWork },
                { key: 'portfolio', component: Services },
                { key: 'services', component: Services },
                { key: 'others', component: PracticeTwo }
            ]
        }
    }

    componentWillMount() {
        axios.get('/api/navbarmenu').then(response => {
            this.setState({
                navbarmenu: response.data
            })
        }).catch(error => {
            if (error.response) {
                alert('Code: ' + error.response.data.error.code +
                    '\r\nMessage: ' + error.response.data.error.message);
                console.log('success', error.message);
            } else {
                console.log('Error', error.message);
            }
        })
    }

    handleSearchIcon(e) {
        this.setState({
            changeSearchIcon: e
        })
    }

    showOrHideDock(e) {
        this.setState({
            showDock: e,
            dockVisible: true
        })
    }

    handleDockVisibleChange() {
        this.setState({
            dockVisible: false
        })
    }

    displayDockMenu() {
        const temp = this.state.dockVisible;
        if (temp) {

            return (
                <div className="dockIcon">
                    <IconSts name="dockIcon" />
                </div>
            );
        } else {
            return (
                <div className="dockIcon" style={{ display: 'none' }}>
                    <IconSts />
                </div>
            );
        }
    }

    DockHere() {
        return (
            <Dock
                position='right'
                size={this.state.size}
                duration={this.state.duration}
                dimMode={this.state.dimMode}
                fluid={this.state.fluid}
                isVisible={this.state.showDock}
                dockStyle={{ background: 'transparent', right: '-500%', position: 'fixed' }}>

                {/* <div className="dockIcon">
                    <IconSts/>                                     
                </div>  */}
                {/* <this.displayDockMenu/> */}
                <div className="dockCloseIcon" onClick={() => { this.showOrHideDock(!this.state.showDock); this.handleDockVisibleChange() }}>
                    <span>
                        <Close width={25} height={25} />
                    </span>
                </div>
                <DockComponent showOrHideDock={this.showOrHideDock} />
            </Dock>
        );
    }

    handleButtonClick() {
        this.props.handleNavToggle(this.state.text);
        this.setState({
            text: !this.state.text,
        })
    }

    handleOverview() {

    }

    handleNavLinkClick() {
        this.setState({
            displayFullNav: false
        })
        // alert(this.state.displayFullNav);
    }

    handleIconClick() {
        this.setState({
            displayFullNav: true
        })
    }

    capitalFirstLetter(st) {
        if (st == null || st == '') {
            return '';
        }
        st = st.toLowerCase();
        return st.charAt(0).toUpperCase() + st.slice(1);
    }

    gotoHomPageComponent() {
        // this.childHomePageComponent.current.handleGetInTouch();

    }

    render() {
        // const sliceString = this.state.string.charAt(0).toUpperCase()+this.state.string.slice(1);
        const renderRouteLink = this.state.navbarmenu.map(navbarmenu => {
            // console.log(this.capitalFirstLetter('DipAk'));


        })
        return (
            <div >
                {/* //removed bg-danger */}
                <Router>
                    <div>
                        <nav id="navbar" className="navbar navbar-expand-lg fixed-top navbar-light">
                            <NavLink id="navbar-icon" to="/" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <IconSts />
                                <div className="stsMainTitle" >Sustainable Technological Solutions</div>
                            </NavLink>

                            {/* <div className="bb" ></div> */}
                            {/* <a id="navbar-icon" className="navbar-brand ">STS</a> */}
                            <div id="vertical-border"></div>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto" id="nav-item">
                                    <li className="nav-item active dropdown">
                                        <NavLink id="nav-link" className="nav-link" to="/about_us" onClick={this.handleNavLinkClick}>
                                            <span id="a-text">About Us</span>
                                            <span className="sr-only">(current)</span>
                                        </NavLink>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <NavLink id="nav-link" className="nav-link" to="/services">
                                            <span id="a-text">Services</span>
                                        </NavLink>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <NavLink id="nav-link" className="nav-link" to="/our_work">
                                            <span id="a-text">Our Work</span>
                                        </NavLink>
                                    </li>
                                    {/* <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span id="a-text">Our Work</span>
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a className="dropdown-item" href="#">Action</a>
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" href="#">Another action</a>
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </li> */}
                                    <li className="nav-item dropdown">
                                        {/* //removed disabled */}
                                        <NavLink id="nav-link" className="nav-link" to="/news">
                                            <span id="a-text">News</span>
                                        </NavLink>
                                    </li>
                                </ul>
                                <button className="btnSearch" onMouseEnter={() => this.handleSearchIcon(true)}
                                    onMouseLeave={() => this.handleSearchIcon(false)}>
                                    <Icon id="search-icon" name="search" size='2em' fill={this.state.changeSearchIcon ? "#9CF5A6" : 'white'}></Icon>
                                </button>
                                <NavLink to="/contact">
                                    <button id="button-get-in-touch"
                                        className="btn btn-outline-success my-2 my-sm-0"
                                        onClick={this.gotoHomPageComponent}>
                                        <span id="btn-text" >
                                            Get in touch
                                        </span>
                                    </button>
                                </NavLink>


                                <button className=" btnNavMenu" onClick={this.showOrHideDock} >
                                    {/* <span style={{
                                            color:'#fff'
                                        }}>
                                        <Menu />
                                    </span> */}
                                    <Menu id="nav-menu-icon" color='#fff' />
                                </button>
                                <this.DockHere />
                            </div>
                        </nav>
                        <Switch>
                            <Route path="/" exact component={HomePageComponent}></Route>
                            <Route path="/services" exact component={Services}></Route>
                            <Route path="/contact" exact component={ContactComponent}></Route>
                            <Route path="/portfolio" exact component={AboutUs}></Route>
                            <Route path="/career" exact component={PracticeTwo}></Route>
                            <Route path="/about" exact component={AboutUs}></Route>
                        </Switch>

                        {/* {renderRouteLink} */}
                        {/* <Route path="/career" exact component={PracticeTwo}></Route>
                        <Route path="/about_us" exact component={AboutUs}></Route>
                        <Route path="/services" exact component={AboutUs}></Route>
                        <Route path="/our_work" exact component={Practice}></Route>
                        <Route path="/contact_us" exact component={ContactComponent}></Route> */}
                    </div>
                </Router>
            </div>
        );
    }
}

class FullWidthModal extends Component {
    render() {
        return (
            <div id="fsModal"
                className="modal animated bounceIn"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel"
                aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">

                        </div>
                        <div className="modal-body">

                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-secondary"
                                data-dismiss="modal">
                                close
                            </button>
                            {/* <button className="btn btn-default">
                                Default
                            </button>
                            <button className="btn btn-primary">
                                Primary
                            </button> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export class IconSts extends Component {
    render() {
        if (this.props.name === 'dockIcon') {
            return (
                //inspire from animation.css
                <span>
                    <div className="box" >
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        {/* TODO: icon here... */}
                        <span >
                            <img id="sts-icon" src={require('../../../assest/icon/stsLogo.png')} style={{ width: '50px', height: '50px' }}></img>
                        </span>
                        {/* style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} */}
                    </div>

                </span>

            );
        }
        return (
            //inspire from animation.css
            <span>
                <div className="box" >
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    {/* TODO: icon here... */}
                    <span >
                        <img src={require('../../../assest/icon/stsLogo.png')} style={{ width: '50px', height: '50px' }}></img>
                    </span>
                    {/* style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} */}
                </div>

            </span>

        );
    }
}